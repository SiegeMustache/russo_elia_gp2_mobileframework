﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GameManagerMobile : Singleton<GameManagerMobile>
{
    public event Action<GameState> ShowCurrentPage;
    public GameState currentState;
    public int score;
    //ui pages change the current game state
    //GM fits and tells the UIM waht ui page to activate

    private void Awake()
    {
        currentState = GameState.MainMenuState;
        ShowCurrentPage?.Invoke(currentState);
    }

    public void ChangeState(GameState state)
    {
        currentState = state;
        ShowCurrentPage?.Invoke(currentState);
    }
}
