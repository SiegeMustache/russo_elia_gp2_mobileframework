﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UiManagerMobile : Singleton<UiManagerMobile>
{
    public List<GameObject> pages;

    private void Awake()
    {
        GameManagerMobile.instance.ShowCurrentPage+=OnShowCurrentPage;
    }

    public void OnShowCurrentPage(GameState state)
    {
        for(int i = 0; i < pages.Count; i++)
        {
            pages[i].SetActive(false);
        }
        if (state == GameState.MainMenuState)
        {
            pages[0].SetActive(true);
        }
        if (state == GameState.GamePlayState)
        {
            pages[1].SetActive(true);
        }
        if (state == GameState.PauseState)
        {
            pages[2].SetActive(true);
        }
        if (state == GameState.GameOverState)
        {
            pages[3].SetActive(true);
        }
    }

}
