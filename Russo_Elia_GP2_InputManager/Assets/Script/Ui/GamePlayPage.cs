﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamePlayPage : MonoBehaviour
{
    public void Pause()
    {
        GameManagerMobile.instance.ChangeState(GameState.PauseState);
    }
    
}
