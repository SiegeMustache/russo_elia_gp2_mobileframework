﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PausePage : MonoBehaviour
{

    public void Restart()
    {
        GameManagerMobile.instance.ChangeState(GameState.GamePlayState);
        Debug.Log("Restarted");
        //invoke a reset method
    }
    public void Resume()
    {
        GameManagerMobile.instance.ChangeState(GameState.GamePlayState);
        Debug.Log("Resumed");
    }
    public void BackMainMenu()
    {
        GameManagerMobile.instance.ChangeState(GameState.MainMenuState);
        
    }
}
