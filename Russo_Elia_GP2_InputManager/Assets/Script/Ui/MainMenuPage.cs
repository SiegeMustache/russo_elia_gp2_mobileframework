﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuPage : MonoBehaviour
{
    public GameObject mainMenu;
    public GameObject options;
    public GameObject credits;

    public void Play()
    {
        GameManagerMobile.instance.ChangeState(GameState.GamePlayState);
    }
    public void Settings()
    {
        mainMenu.SetActive(false);
        credits.SetActive(false);
        options.SetActive(true);
    }
    public void Credits()
    {
        mainMenu.SetActive(false);
        options.SetActive(false);
        credits.SetActive(true);
    }
}
