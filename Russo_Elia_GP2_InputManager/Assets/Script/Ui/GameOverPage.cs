﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverPage : MonoBehaviour
{

    public void Restart()
    {
        GameManagerMobile.instance.ChangeState(GameState.GamePlayState);
    }
    public void BackToMenu()
    {
        GameManagerMobile.instance.ChangeState(GameState.MainMenuState);
    }
    public void ShowAds()
    {
        AdsManager.instance.ShowRewardedAd();
    }
}
