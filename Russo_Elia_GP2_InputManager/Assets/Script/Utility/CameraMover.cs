﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMover : MonoBehaviour
{

    public float mainMenuX;
    public float playScreenX;
    public float poolScreenX;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GoToScreen(float menuX)
    {
        transform.position = new Vector3(menuX, transform.position.y, transform.position.z);
    }

    public void NextScreen()
    {
        if(transform.position.x == playScreenX)
        {
            GoToScreen(poolScreenX);
        }
    }

    public void PreviousScreen()
    {
        if(transform.position.x == poolScreenX)
        {
            GoToScreen(playScreenX);
        }
    }

    public void GoToPlayMenu()
    {
        GoToScreen(playScreenX);
    }

    public void GoToMainMenu()
    {
        GoToScreen(mainMenuX);
    }
}
