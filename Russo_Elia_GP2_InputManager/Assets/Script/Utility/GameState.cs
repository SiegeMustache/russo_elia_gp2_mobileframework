﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameState 
{
    MainMenuState,
    GamePlayState,
    PauseState,
    GameOverState
}
