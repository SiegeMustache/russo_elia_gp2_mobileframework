﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipeController : MonoBehaviour
{
    
    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnSwipe(Vector2 startPosition, Vector2 endPosition)
    {
        if(startPosition.x < endPosition.x + InputManager.instance.swipeControlThreshold)
        {
            SwipeLeft();
        }
        else if(startPosition.x > endPosition.x + InputManager.instance.swipeControlThreshold)
        {
            SwipeRight();
        }
        else if(startPosition.y < endPosition.y + InputManager.instance.swipeControlThreshold)
        {
            SwipeUp();
        }
        else if(startPosition.y > endPosition.y + InputManager.instance.swipeControlThreshold)
        {
            SwipeDown();
        }
    }

    private void SwipeLeft()
    {
        AudioManager.instance.PlayClip(AudioManager.instance.audioClips[0]);
        Camera.main.GetComponent<CameraMover>().PreviousScreen();
    }

    private void SwipeRight()
    {
        AudioManager.instance.PlayClip(AudioManager.instance.audioClips[0]);
        Camera.main.GetComponent<CameraMover>().NextScreen();
    }

    private void SwipeUp()
    {

    }

    private void SwipeDown()
    {

    }
}
