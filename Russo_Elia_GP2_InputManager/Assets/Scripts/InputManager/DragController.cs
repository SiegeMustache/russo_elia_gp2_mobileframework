﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragController : MonoBehaviour
{

    [HideInInspector]
    public Vector2 startPositionDrag;

    
    // Update is called once per frame
    void Update()
    {
        if (this.gameObject == InputManager.instance.objectDragged)
        {
            this.gameObject.transform.position = new Vector3(Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position).x, Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position).y, 0);
        }
    }

}
