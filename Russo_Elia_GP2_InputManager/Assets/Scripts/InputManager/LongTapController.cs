﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LongTapController : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        if(this.gameObject == InputManager.instance.objectInLongTap)
        {
            this.gameObject.GetComponent<Spawner>().Spawn();
        }
    }
}
