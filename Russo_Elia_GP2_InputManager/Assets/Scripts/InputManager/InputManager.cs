﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : Singleton<InputManager>
{
    public float swipeThresholdSpeed;
    public float longTapThresholdTime;
    public float swipeControlThreshold;
    public float tapMovementThrshold;

    [HideInInspector]
    public GameObject objectInLongTap;
    [HideInInspector]
    public GameObject objectDragged;
    [HideInInspector]
    public GameObject objectPinched;
    [HideInInspector]
    public Touch[] touch;
    [HideInInspector]
    public float currentDelta;

    private Vector2 startPosition;
    private Vector2 endPosition;
    private Vector2 startPosition0;
    private Vector2 startPosition1;
    private float startTime;
    private float endTime;

    private float startDelta;
    private bool isDragging = false;
    

    private void Start()
    {
        ResetTouchInput();
        touch = new Touch[2];
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.touchCount == 1)
        {
            touch[0] = Input.GetTouch(0);
            SaveManager.instance.score++;
            switch(touch[0].phase)
            {
                case TouchPhase.Began:
                    {
                        startPosition = touch[0].position;
                        startTime = Time.time;
                        EnableTouchDrag(startPosition, touch[0]);
                        break;
                    }
                case TouchPhase.Moved:
                    {
                        if(CalculateSpeed(touch[0].deltaPosition, touch[0].deltaTime) <= swipeThresholdSpeed && isDragging == false)
                        {
                            EnableTouchDrag(startPosition, touch[0]);
                            isDragging = true;
                        }
                        else if(CalculateSpeed(touch[0].deltaPosition, touch[0].deltaTime) > swipeThresholdSpeed && isDragging == false)
                        {
                            isDragging = false;
                        }

                        if(objectInLongTap != null && touch[0].deltaPosition.magnitude > tapMovementThrshold)
                        {
                            EndLongTap();
                            ResetTouchInput();
                        }
                        break;
                    }
                case TouchPhase.Stationary:
                    {
                        if(Time.time - startTime >= longTapThresholdTime && objectInLongTap == null)
                        {
                            LongTap(touch[0]);
                        }

                        break;
                    }
                case TouchPhase.Ended:
                    {
                        endPosition = touch[0].position;
                        endTime = Time.time;
                        if(Vector2.Distance(endPosition, startPosition) < tapMovementThrshold)
                        {
                            if(objectInLongTap != null)
                            {
                                EndLongTap();
                            }
                            else if(objectInLongTap == null && endTime - startTime <= longTapThresholdTime)
                            {
                                Tap(touch[0]);
                            }
                            ResetTouchInput();
                        }
                        else if(endPosition != startPosition && isDragging == false)
                        {
                            if(CalculateSpeed(startPosition, endPosition, startTime, endTime) >= swipeThresholdSpeed)
                            {
                                TouchSwipe(startPosition, endPosition);
                            }
                            ResetTouchInput();
                        }
                        else if(objectDragged != null)
                        {
                            DisableTouchDrag();
                            ResetTouchInput();
                        }
                        break;
                    }
                case TouchPhase.Canceled:
                    {
                        ResetTouchInput();
                        break;
                    }
            }
        }
        else if(Input.touchCount >= 2)
        {
            touch[0] = Input.GetTouch(0);
            touch[1] = Input.GetTouch(1);
            switch(touch[0].phase)
            {
                case TouchPhase.Began:
                    {
                        startPosition0 = touch[0].position;
                        break;
                    }
                case TouchPhase.Moved:
                    {
                        break;
                    }
                case TouchPhase.Stationary:
                    {
                        break;
                    }
                case TouchPhase.Ended:
                    {
                        DisableTouchPinch();
                        ResetTouchInput();
                        break;
                    }
                case TouchPhase.Canceled:
                    {
                        ResetTouchInput();
                        break;
                    }
            }
            switch(touch[1].phase)
            {
                case TouchPhase.Began:
                    {
                        startPosition1 = touch[1].position;
                        startDelta = Vector2.Distance(startPosition0, startPosition1);
                        EnableTouchPinch(touch[0], touch[1]);
                        break;
                    }
                case TouchPhase.Moved:
                    {
                        currentDelta = (Vector2.Distance(touch[0].position, touch[1].position)) / startDelta;
                        TouchPinch(currentDelta);
                        break;
                    }
                case TouchPhase.Stationary:
                    {
                        TouchPinch(currentDelta);
                        break;
                    }
                case TouchPhase.Ended:
                    {
                        DisableTouchPinch();
                        ResetTouchInput();
                        break;
                    }
                case TouchPhase.Canceled:
                    {
                        ResetTouchInput();
                        break;
                    }
            }
        }
    }

    private void Tap(Touch touch)
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(touch.position);

        if (Physics.Raycast(ray, out hit))
        {
            Transform objectHit = hit.transform;
            if(objectHit.gameObject.GetComponent<TapController>())
            {
                objectHit.gameObject.GetComponent<TapController>().OnTap();
            }
        }
    }

    private void LongTap(Touch touch)
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(touch.position);

        if (Physics.Raycast(ray, out hit))
        {
            Transform objectHit = hit.transform;
            if(objectHit.gameObject.GetComponent<LongTapController>())
            {
                objectInLongTap = objectHit.gameObject;
            }
        }
    }

    private void EndLongTap()
    {
        objectInLongTap = null;
    }

    private void EnableTouchDrag(Vector2 startPosition, Touch touch)
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(startPosition);

        if (Physics.Raycast(ray, out hit))
        {
            Transform objectHit = hit.transform;
            if (objectHit.gameObject.GetComponent<DragController>())
            {
                objectDragged = objectHit.gameObject;
                objectDragged.GetComponent<DragController>().startPositionDrag = startPosition;
            }
        }
    }

    private void DisableTouchDrag()
    {
        objectDragged = null;
    }

    private void TouchSwipe(Vector2 startPosition, Vector2 endPosition)
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(startPosition);

        if (Physics.Raycast(ray, out hit))
        {
            Transform objectHit = hit.transform;
            if (objectHit.gameObject.GetComponent<SwipeController>())
            {
                objectHit.gameObject.GetComponent<SwipeController>().OnSwipe(startPosition, endPosition);
            }
        }
    }

    private void EnableTouchPinch(Touch touch0, Touch touch1)
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(touch0.position);

        if (Physics.Raycast(ray, out hit))
        {
            Transform objectHit = hit.transform;
            if (objectHit.gameObject.GetComponent<SwipeController>())
            {
                objectPinched = objectHit.gameObject;
            }
            else if(!objectHit.gameObject.GetComponent<SwipeController>())
            {
                ray = Camera.main.ScreenPointToRay(touch1.position);
                if(Physics.Raycast(ray, out hit))
                {
                    objectPinched = objectHit.gameObject; 
                }
            }
        }
    }

    private void TouchPinch(float currentDelta)
    {
        objectPinched.GetComponent<PinchController>().OnPinch(currentDelta);
    }

    private void DisableTouchPinch()
    {
        objectPinched = null;
    }

    private float CalculateSpeed(Vector2 startPosition, Vector2 endPosition, float startTime, float endTime)
    {
        float distance = Vector2.Distance(startPosition, endPosition);
        float timeElapsed = endTime - startTime;

        float speed = distance / timeElapsed;

        return speed;
    }

    private float CalculateSpeed(Vector2 deltaPosition, float deltaTime)
    {
        float speed = deltaPosition.magnitude / deltaTime;
        return speed;
    }

    private void ResetTouchInput()
    {
        startPosition = Vector2.zero;
        startPosition0 = Vector2.zero;
        startPosition1 = Vector2.zero;
        endPosition = Vector2.zero;
        startTime = 0;
        endTime = 0;
        startDelta = 0;
        currentDelta = 0;
        objectDragged = null;
        objectPinched = null;
        objectInLongTap = null;
    }
}
