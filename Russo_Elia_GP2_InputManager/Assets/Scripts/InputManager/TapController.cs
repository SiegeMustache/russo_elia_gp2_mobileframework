﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TapController : MonoBehaviour
{
    public Material[] materials;
    public bool isDoubleTap;
    public float doubleTapTime;

    private int index;
    private float doubleTapTimer;
    private bool isTapped;
    private MeshRenderer mr;

    private void Start()
    {
        doubleTapTimer = 0;
        index = 0;
        mr = GetComponent<MeshRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if(isTapped)
        {
            doubleTapTimer += Time.deltaTime;
        }
        mr.material = materials[index];
    }

    public void OnTap()
    {
        AudioManager.instance.PlayClip(AudioManager.instance.audioClips[0]);
        if (isDoubleTap == true && isTapped == false)
        {
            isTapped = true;
            doubleTapTimer = 0;
        }
        else if(isDoubleTap == true && isTapped == true)
        {
            if(doubleTapTimer < doubleTapTime)
            {
                if (index < materials.Length)
                {

                    index++;
                    if (index == materials.Length)
                    {
                        index = 0;
                    }
                }
            }
            isTapped = false;
        }
        else if(isDoubleTap == false)
        {
            if(index < materials.Length)
            {

                index++;
                if(index == materials.Length)
                {
                    index = 0;
                }
            }
        }
    }
}
