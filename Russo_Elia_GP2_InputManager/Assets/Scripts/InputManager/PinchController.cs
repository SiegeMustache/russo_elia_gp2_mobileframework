﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PinchController : MonoBehaviour
{
    public float originalScaleX;
    public float originalScaleY;

    private void Start()
    {
        originalScaleX = this.gameObject.transform.localScale.x;
        originalScaleY = this.gameObject.transform.localScale.y;
    }
    // Update is called once per frame
    void Update()
    {
        if(this.gameObject == InputManager.instance.objectPinched)
        {
            OnPinch(InputManager.instance.currentDelta);
        }
    }

    public void OnPinch(float currentDelta)
    {
        this.gameObject.transform.localScale = new Vector3(originalScaleX * currentDelta, originalScaleY * currentDelta, 1);
    }

    public void UpdateScale()
    {
        originalScaleX = this.gameObject.transform.localScale.x;
        originalScaleY = this.gameObject.transform.localScale.y;
    }
}
