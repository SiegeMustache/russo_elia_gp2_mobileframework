﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SaveManager : Singleton<SaveManager>
{
    [HideInInspector]
    public int score;
    public Text text;

    // Start is called before the first frame update
    void Start()
    {
        LoadData();
    }

    // Update is called once per frame
    void Update()
    {
        text.text = score.ToString();
    }

    public void SaveData()
    {
        PlayerPrefs.SetInt("Score", score);
    }

    public void LoadData()
    {
        if (PlayerPrefs.HasKey("Score"))
        {
            score = PlayerPrefs.GetInt("Score");
        }
    }
}
