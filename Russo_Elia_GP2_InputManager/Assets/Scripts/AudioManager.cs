﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : Singleton<AudioManager>
{
    public AudioClip[] audioClips;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PlayClip(AudioClip clip)
    {
        Debug.Log("PLAYED");
        GameObject pulledSource = ObjectPooler.instance.GetObjectFromPool("Audio");

        pulledSource.transform.position = transform.position;
        AudioSource mySource = pulledSource.GetComponent<AudioSource>();

        mySource.clip = clip;

        pulledSource.SetActive(true);
    }
}
