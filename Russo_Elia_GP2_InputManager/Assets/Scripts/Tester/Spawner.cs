﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    float t_spawnCooldown;
    GameObject currentSpawn;

    // Start is called before the first frame update
    void Start()
    {
        t_spawnCooldown = 0.9f;
    }

    // Update is called once per frame
    void Update()
    {
        t_spawnCooldown -= Time.deltaTime;
    }

    public void Spawn()
    {
        if(t_spawnCooldown <= 0)
        {
            currentSpawn = ObjectPooler.instance.GetObjectFromPool("Capsule");
            currentSpawn.SetActive(true);
            currentSpawn.transform.position = transform.position;
            t_spawnCooldown = 0.1f;
        }
    }
}
