﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SolidBehavior : MonoBehaviour
{
    private float timer;

    private void OnEnable()
    {
        timer = 5f;
    }

    // Update is called once per frame
    void Update()
    {
        timer -= Time.deltaTime;
        if(timer <= 0)
        {
            this.gameObject.SetActive(false);
        }
    }
   
}
